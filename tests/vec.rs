#![cfg(feature = "vec")]

extern crate bakasable;
use bakasable::vec;

#[test]
fn remove() {
    let mut v = vec![1,2,2,3];
    vec::remove(&mut v, 2);
    assert_eq!(v[..], [1,3]);

    vec::remove(&mut v, 4);
    assert_eq!(v[..], [1,3]);
}

#[test]
fn find() {
    let v = vec![1,3,1];

    assert_eq!(vec::find_idx(&v, 1), Some(0));
    assert_eq!(vec::find_idx(&v, 2), None);
    assert_eq!(vec::find_idx(&v, 3), Some(1));
}

#[test]
fn filter() {
    assert_eq!(vec::filter(vec![Some(1),None,Some(3)]), vec![1,3])
}

#[test]
fn split_at() {
    assert_eq!(vec::split_at(&vec![1,2,3,4,5], 3), (vec![1,2], vec![4,5]))
}

#[test]
fn normalize() {
    let mut v = vec![1.0, 1.0];
    vec::normalize(&mut v);
    assert_eq!(v, vec![0.5, 0.5])
}

#[test]
fn add_prefix() {
    let mut words : Vec<String> = vec!["dox".into(), "pluie".into()];
    vec::add_prefix(&String::from("para"), &mut words);
    assert_eq!(words, vec![String::from("paradox"), String::from("parapluie")])
}
