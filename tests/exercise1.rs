extern crate bakasable;

#[cfg(feature = "exercise1")]
use bakasable::exercise1::{five, twelve};

#[cfg(feature = "exercise1")]
#[test]
fn five_returns_five() {
    assert_eq!(five(), 5);
}

#[cfg(feature = "exercise1")]
#[test]
fn twelve_returns_twelve() {
    assert_eq!(twelve(), 12);
}
