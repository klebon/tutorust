#![cfg(feature = "list")]

extern crate bakasable;
use bakasable::stack::Stack;

#[test]
fn iterate() {
    let mut st = Stack::new();

    st.push(1);
    st.push(2);
    st.push(3);
    st.push(4);

    let v : Vec<i32> = st.iter().copied().collect();
    assert_eq!(vec![4,3,2,1], v);
}
