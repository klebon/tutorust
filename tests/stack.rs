#![cfg(feature = "stack")]

extern crate bakasable;

use bakasable::stack::Stack;

#[test]
fn stack_works() {
    let mut stack : Stack<i32> = Stack::new();

    stack.push(1);
    stack.push(3);

    assert_eq!(stack.pop(), Some(3));
    stack.push(2);
    assert_eq!(stack.pop(), Some(2));
    assert_eq!(stack.pop(), Some(1));
    assert_eq!(stack.pop(), None);
    stack.push(0);
    assert_eq!(stack.pop(), Some(0));
    assert_eq!(stack.pop(), None);
}
