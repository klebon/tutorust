extern crate bakasable;

use bakasable::exercise2::Point;

#[cfg(feature = "exercise2")]
#[test]
fn point_implements_from() {
    let point = Point::from((3, 4));

    assert_eq!(point, Point { x: 3, y: 4 });
}
