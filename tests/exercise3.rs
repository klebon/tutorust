extern crate bakasable;

#[cfg(feature = "exercise3")]
use bakasable::exercise2::{sum, fibo};

#[cfg(feature = "exercise3")]
#[test]
fn test_fibo() {
    assert_eq!(fibo(0), 0);
    assert_eq!(fibo(1), 1);
    assert_eq!(fibo(5), 5);
    assert_eq!(fibo(6), 8);
}

#[cfg(feature = "exercise3")]
#[test]
fn empty_sum_is_zero() {
    assert_eq!(sum(&[]), 0);
}

#[cfg(feature = "exercise3")]
#[test]
fn test_sum() {
    assert_eq!(sum(&[1, 2, 3, 4]), 10);
    assert_eq!(sum(&[1, 2]), 3);
}
