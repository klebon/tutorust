#![cfg(feature = "stack")]

// The exposed type
pub struct Stack<T> {
    head : _, // TODO: define the type of head
}

// the actual nodes containing the stack's values
struct Node<T> {
    value : T,
    next : _, // TODO: define the type of next
}

// Stack methods implementation
impl<T> Stack<T> {

    // Creates an empty stack
    pub fn new() -> Stack<T> { Stack {} }
    
    // push a value on the stack
    pub fn push(&mut self, elem:T) {
    }

    // pop a value off the stack
    pub fn pop(&mut self) -> Option<T> {
        None
    }
}
