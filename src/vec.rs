#![cfg(feature = "vec")]

/**
 *
 * remove all occurrences of [value] from [vec]
 *
 */
pub fn remove(vec : &mut Vec<i32>, value : i32) {
}

/**
 *
 * Given a [vec] and an [element] of type T
 *
 * return the index of the first occurrence of [element] in [vec], if any
 * return the value None if there are none
 * 
 */
pub fn find_idx<T : PartialEq>(vec : &Vec<T>, element : T) -> Option<usize> {
    None
}

/**
 *
 * Given a collection of Option<T>, return a Vec containing all "existing"
 * elements in the collection.
 *
 * filter( [Some(0), None, Some(2)] ) -> [ &0, &2 ]
 *
 */
pub fn filter(vec : Vec<Option<i32>>) -> Vec<i32> {
    vec![]
}

/**
 *
 * Splits a [vec] into 2 parts: (before, after)
 *
 * before contains all elements of [vec] before [element]
 * after contains all elements of [vec] after it
 *
 * split_at( [1,2,3,4,5], 3 ) -> ( [1,2], [4,5] )
 *
 */
pub fn split_at(vec:&Vec<i32>, element:i32) -> (Vec<i32>, Vec<i32>) {
    (vec![], vec![])
}


/**
 *
 * normalize all values contained in [vec].
 *
 * e.g.:
 * ```
 * let mut v = vec![1.0, 1.0];
 *
 * normalize(&mut v)
 * assert_eq!(v, vec![0.5, 0.5])
 * ```
 *
 */
pub fn normalize(vec:&mut Vec<f32>) {
}

/**
 * 
 * Inserts a the String [pre] before all Strings contained in [vec]
 *
 * add_prefix( "para", ["dox", "pluie"] ) -> ["paradox", "parapluie"]
 */
pub fn add_prefix(pre:&String, words:&mut Vec<String>) {
}

