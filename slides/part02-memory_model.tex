%! TEX root = tuto.tex

\section{Memory model}

\begin{frame}{As <<simple>> as C}
  Rust memory model is based on C, but <<more rigid>>.\\
  Local variables are allocated on the stack.\\
  Dynamic structures (size known at runtime) are allocated on the heap.
\end{frame}

\begin{frame}[fragile]{Dynamically sized stuctures}
  We often want to work on arrays or lists with dynamic size.
  \bigskip

  \begin{block}{Main dynamic structures}
    \begin{itemize}
      \item \texttt{String}: a dynamic \texttt{str}
      \item \texttt{Vec<T>}: a dynamic array (\texttt{[T]})
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Using String}
  \begin{rust}
    // create String from static &str
    let mut hello = String::from("Hello");

    // modify their content
    hello += ", World";
    hello.push('!');

    // read/print it
    println!("{}", hello);
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Using Vec<T>}
  \begin{rust}
    // create empty or non-empty vecs
    let empty = Vec::new();
    let mut vec = vec![1, 2, 3, 4];
    
    // modify their content
    vec.push(5);
    vec.remove(0);

    // iterate over a Vec à la Java
    for i in 0..vec.len() {
      println!("{}", vec[i]);
    }

    // or with iterators
    for v in vec {
      println!("{}", v);
    }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Behind the scene}
  \texttt{String} or \texttt{Vec<T>} are juste wrappers around heap-allocated
  memory.
  \bigskip

  When creating a \texttt{String}, here is what happens:
  \begin{rust}
    let x = String::from("Hello");
  \end{rust}
  \bigskip

  \begin{figure}
    \centering
    \begin{tikzpicture}
      \node (x) at (-.5cm,-.25cm) {x};
      \node[draw,text width=2cm,text height=.2cm,align=center] (xptr) at (1cm,0) {ptr};
      \node[draw,text width=2cm,text height=.2cm,align=center] (xsize) at (1cm,-.5cm) {size};

      \node[draw,text width=1cm,text height=.2cm,align=center] (mem1) at (5cm,0) {H};
      \node[draw,text width=1cm,text height=.2cm,align=center] (mem2) at (5cm,-.5cm) {e};
      \node[draw,text width=1cm,text height=.2cm,align=center] (mem3) at (5cm,-1cm) {l};
      \node[draw,text width=1cm,text height=.2cm,align=center] (mem4) at (5cm,-1.5cm) {l};
      \node[draw,text width=1cm,text height=.2cm,align=center] (mem5) at (5cm,-2cm) {o};

      \path[every node/.style={font=\sffamily\small}]
        (xptr) edge[-latex] node [right] {} (mem1);
    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}[fragile]{Behind the scene}
  \texttt{String} or \texttt{Vec<T>} are juste wrappers around heap-allocated
  memory.
  \bigskip

  What would happen if we copy a \texttt{String} ?
  \begin{rust}
    let x = String::from("Hello");
    let y = x;
  \end{rust}
  \bigskip

  \pause%
  One possible outcome:
  \begin{figure}
    \centering
    \begin{tikzpicture}
      \node (x) at (-.5cm,-.25cm) {x};
      \node[draw,text width=2cm,text height=.2cm,align=center] (xptr) at (1cm,0) {ptr};
      \node[draw,text width=2cm,text height=.2cm,align=center] (xsize) at (1cm,-.5cm) {size};

      \node[draw,text width=1cm,text height=.2cm,align=center] (mem1) at (5cm,0) {H};
      \node[draw,text width=1cm,text height=.2cm,align=center] (mem2) at (5cm,-.5cm) {e};
      \node[draw,text width=1cm,text height=.2cm,align=center] (mem3) at (5cm,-1cm) {l};
      \node[draw,text width=1cm,text height=.2cm,align=center] (mem4) at (5cm,-1.5cm) {l};
      \node[draw,text width=1cm,text height=.2cm,align=center] (mem5) at (5cm,-2cm) {o};

      \node (y) at (-.5cm,-2.25cm) {y};
      \node[draw,text width=2cm,text height=.2cm,align=center] (yptr) at (1cm,-2cm) {ptr};
      \node[draw,text width=2cm,text height=.2cm,align=center] (ysize) at (1cm,-2.5cm) {size};

      \coordinate (mid) at ($(yptr)!0.5!(mem1)$) {};

      \path[every node/.style={font=\sffamily\small}]
        (xptr) edge[-latex] node [right] {} (mem1)
        (yptr.east) edge[bend right=30] node [left] {} (mid)
        (mid) edge[-latex,bend left=45] node [left] {} (mem1.west);
    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}[fragile]{Behind the scene}
  It could lead to memory corruption.
  \begin{rust}
    {
      // x allocates memory
      let x = String::from("Hello");

      {
        // y copies x's stack data
        let y = x;
      } // y goes out of scope, freeing heap data

      x += ", World!"; // use after free
    } // x goes out of scope: double free
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Semantics of affectation}
  To prevent people from making mistakes when creating or using these
  <<complex>> types, Rust \textbf{never copies} data when passing its value.
  \bigskip

  Instead, it \textbf{moves} data:
  \begin{rust}
    let hello = String::from("Hello");
    let mut hello_me = hello; // hello_me "steals" hello's data

    hello_me += " me!";

    // error: hello is invalidated
    println!("{} {}", hello_me, hello);
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Ownership}
  When we give a value to a variable, we say that the variable \emph{owns} the value.
  
  \begin{block}{Ownership Rule}
  A value can only have 1 owner.
  \end{block}
  \bigskip

  \begin{rust}
    // empty owns value "String::new()"
    let empty = String::new();
    
    // x becomes the new owner
    let x = empty;
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Borrowing ownership}
  Variables are very kind in Rust.\\
  We can borrow their content (reference it).
  \begin{rust}
    fn print_reference(s:&String) {
      println!("{}", s)
    }

    let x = String::from("some text");

    print_reference(&x); // x is borrowed, not moved

    x += ", some other text"; // x is still valid
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Mutable borrow}
  We can also borrow a mutable value to modify it in place.
  Let us analyse the \emph{trait} providing the operator \texttt{+=}.
  \begin{rust}
    trait AddAssign {
      fn add_assign(&mut self, rhs:Self);
    }
  \end{rust}
  \bigskip%

  The function we have to implement takes a mutable reference (\texttt{\&mut}) to
  our value. This way, we can modify the value we borrowed.

  \begin{rust}
    let mut s = String::new();

    s += "hello"; // s.add_assign("hello");
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Borrow rules}
  At any instant, a value must have either:
  \begin{itemize}
    \item[$-$] One mutable reference
    \item[$-$] Any number of immutable reference
  \end{itemize}
  \bigskip%

  \begin{columns}[t]
    \vrule{}
    \begin{column}{0.45\textwidth}
      This is legal:
      \begin{rust}
        let mut s = String::new();
        let v = [&s, &s];

        println!("{}", v[0]);
      \end{rust}
    \end{column}
    \vrule{}
    \begin{column}{0.45\textwidth}
      This is not:
      \begin{rust}
        let mut v = vec![1];
        let item = v.last();

        v.push(2);

        println!("{}", item);
      \end{rust}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[t,fragile]{Move/Borrow exercises}
  According to Rust's semantics, is this code legal?
  \bigskip

  \begin{rust}
    let mut vec = vec![1,2,3];
    let mut sum = 0;

    for v in &vec { // borrow vec
      sum += v
    }

    println!("{}", vec);
  \end{rust}
  \bigskip

  \pause%
  \textbf{Yes}
\end{frame}

\begin{frame}[t,fragile]{Move/Borrow exercises}
  According to Rust's semantics, is this code legal?
  \bigskip

  \begin{rust}
    let mut vec = vec![1,2,3];
    let mut sum = 0;

    for v in vec { // move vec
      sum += v
    }

    println!("{}", sum);
  \end{rust}
  \bigskip

  \pause%
  \textbf{Yes}
\end{frame}

\begin{frame}[t,fragile]{Move/Borrow exercises}
  According to Rust's semantics, is this code legal?
  \bigskip

  \begin{rust}
    let mut vec = vec![1,2,3];
    let mut sum = 0;

    for v in vec { // move vec
      sum += v
    }

    println!("sum of {} = {}", vec, sum);
  \end{rust}
  \bigskip

  \pause%
  \textbf{No}
\end{frame}

\begin{frame}[t,fragile]{Move/Borrow exercises}
  According to Rust's semantics, is this code legal?
  \bigskip

  \begin{rust}
    let mut vec = vec![0,1,2];

    for v in &vec { // borrow vec
      vec.push(v)
    }

    println!("{}", vec)
  \end{rust}
  \bigskip

  \pause%
  \textbf{No}
\end{frame}

\begin{frame}[t,fragile]{Move/Borrow exercises}
  According to Rust's semantics, is this code legal?
  \bigskip

  \begin{rust}
    let mut vec = vec![1,2,3];

    for i in 0..vec.len() { // iterate over indices
      vec.push(i)
    }

    println!("{}", vec)
  \end{rust}
  \bigskip

  \pause%
  \textbf{Yes}
\end{frame}

\begin{frame}[t,fragile]{Move/Borrow exercises}
  According to Rust's semantics, is this code legal?
  \bigskip

  \begin{rust}
    let mut x = 10;
    let mut y = x;

    y += 10;
    x = 15;

    println!("{} {}", x, y);
  \end{rust}
  \bigskip

  \pause%
  \textbf{It <<shouldn't>>, but it is.}\\
  If a type implements the \texttt{Copy} trait, implicit copy can be used
  instead of move. All numeric types implement \texttt{Copy}.
\end{frame}

\begin{frame}[fragile]{Clone trait}
  \begin{rust}
    trait Clone {
      fn clone(&self) -> Self;
    }
  \end{rust}
  \bigskip

  Implementing \texttt{Clone} means a type can clone itself, returning a new
  \textbf{value} from a reference to itself. For instance, \texttt{Vec}
  implements \texttt{Clone}.

  \begin{rust}
    let mut x = vec![1, 2, 3];
    // explicit copy
    let y = x.clone();

    x.push(4);
    println!("{:?} {:?}", x, y); // [1, 2, 3, 4] [1, 2, 3]
  \end{rust}
\end{frame}

\begin{frame}{Exercises with Vec and String}
  Open the file \texttt{src/vec.rs} and impement its functions.
  \bigskip%

  To test if your implementation works, type:\\
  \texttt{cargo test ----features vec}
\end{frame}

\begin{frame}[fragile]{Dynamic allocation}
  Until now, we never used the heap ourselves.\\
  Let us allocate our first heap chunk:
  \bigskip

  \begin{minted}{Rust}
    let heap_int : Box<i64> = Box::new(10);
  \end{minted}
  \bigskip

  That's it! We have a reference to a dynamically allocated \texttt{i64}.
\end{frame}

\begin{frame}[fragile]{Drop}
  We now know how to do dynamic allocation. But how to free this allocated chunk?
  \bigskip%

  Behold, the \texttt{Drop} trait:
  \begin{rust}
    trait Drop {
      fn drop(&mut self);
    }
  \end{rust}
  \bigskip%

  This trait provides only one function which acts as a destructor.
  This function is called when the variable \emph{owning} the value goes out of
  scope.
\end{frame}

\begin{frame}[fragile]{Drop: example}
  When droped, a \texttt{Box} frees its memory.
  \bigskip%
  \begin{rust}
    fn allocate_and_free()
    {
      let p : Box<i32> = Box::new(10); <| allocate p
      println!("{}", p);                |
    }                                  <| drop(p)
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Boxes are like pointers}
  Rust type system allows you to use \emph{Boxed} values as if they were normal
  values.
  \bigskip

  \begin{rust}
    let mut ptr = Box::new(10);

    let val = *ptr; // deref

    *ptr += val; // modify

    println!("{}", ptr); // print referenced value
  \end{rust}
\end{frame}

\section{Finaly: Our first data structure}

\begin{frame}[fragile]{A linked stack}
  Let's use what we learned to implement a simple \texttt{Stack}.\\
  Here are the methods we will impement:
  \bigskip

  \begin{rust}
    // creates an empty stack
    fn new() -> Stack<T>;

    // puts an element on the stack
    fn push(&mut self, e:T);

    // pops an element off the stack
    fn pop(&mut self) -> T;
  \end{rust}
  \bigskip

  The structure's skeleton is in \texttt{src/stack.rs}.
\end{frame}

\begin{frame}[fragile]{A linked stack: defining the type}

  Each <<Node>> is allocated on the heap.\\
  \only<2->{
    -- Use the \texttt{Box} type.
    \bigskip
  }

  A stack node has either \textbf{some element} after it, or \textbf{none}\\
  \pause[3]
  -- Use the \texttt{Option} type.
  \begin{rust}
    enum Option<T> {
      None,
      Some(T),
    }
  \end{rust}
  \bigskip

  \pause[4]
  Our links can be represented with the type:
  \begin{rust}
    type Link<T> = Option<Box<Node<T>>>;
  \end{rust}
\end{frame}

\begin{frame}[fragile]{A linked stack: new}
  We want to hide the internal node system, so the user will ne be able to
  create ill-formed \texttt{Stack} structures. We will give users 1 function to
  create an empty one.
  \bigskip%

  \pause%
  \begin{rust}
    fn new() -> Stack<T> {
      Stack {
        head : None
      }
    }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{A linked stack: push}
  This is the first tricky function.\\
  At first, you would want to do something like this:
  \bigskip

  \begin{rust}
    fn push(&mut self, elt:T) {
      // create a new node
      let new_head = Box::new(
        Node {
          value : elt,
          next : self.head,  // <- Error: self.head moved here
        }
      );

      // put it at the top
      self.head = new_head   // <- but used here after
    }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{A linked stack: push}
  To prevent it, we need a function from the \texttt{Option} type.
  \bigskip

  \begin{rust}
    fn take(&mut self) -> Self;
  \end{rust}
  \bigskip

  Rust's documentation tells us: Takes the value out of the option, leaving a
  None in its place.

  \pause%
  \begin{figure}
    \centering
    \includegraphics[scale=0.2]{indiana_option_take.png}
    \caption{A rust programmer using Option::take}
  \end{figure}
\end{frame}

\begin{frame}[fragile]{A linked stack: push fixed}
  This is the first tricky function.\\
  At first, you would want to do something like this:
  \bigskip

  \begin{rust}
    fn push(&mut self, elt:T) {
      // create a new node
      let new_head = Box::new(
        Node {
          value : elt,
          next : self.head.take(), // Indy move
        }
      );

      // put it at the top
      self.head = new_head
    }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{A linked stack: pop}
  Let us start to implement pop. First we must check whether the stack is empty
  or not. This is done by checking its head.
  \bigskip

  \pause
  \begin{rust}
    fn pop(&mut self) -> Option<T> {
      match self.head {
        Some(node) => { /* code if the stack is not empty */ },
        None => { /* code if the stack is empty */ },
      }
    }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{A linked stack: pop}
  Stack empty ? No value to pop, return \texttt{None}.
  \bigskip

  \begin{rust}
    fn pop(&mut self) -> Option<T> {
      match self.head {
        Some(node) => { /* code if the stack is not empty */ },
        None => { None },
      }
    }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{A linked stack: pop}
  Stack not empty ? Make the stack point to the next item, and return the old
  head's value.
  \bigskip

  \pause%
  \begin{rust}
    fn pop(&mut self) -> Option<T> {
      match self.head {
        Some(node) => {
          self.head = node.next;
          Some(node.value)
        },
        None => { None },
      }
    }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{A linked stack: pop}
  Ouch, the compiler complains again because we borrowed \texttt{self.head}.\\
  Use the take function to safely steal ownership of the old head.
  \bigskip

  \begin{rust}
    fn pop(&mut self) -> Option<T> {
      match self.head.take() {
        Some(node) => {
          self.head = node.next;
          Some(node.value)
        },
        None => { None },
      }
    }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Stack finished!}
  We created our stack !
  \bigskip

  Wait... Don't we have to implement the \texttt{Drop} trait to free data?
  \bigskip

  No! We won't need it, \texttt{Box} will handle it for us.
  \bigskip

  \begin{figure}
    \centering
    \begin{tikzpicture}
      \node[draw] (stack) at (0,0) { Stack };
      \node[draw] (na) at (2,0) { Node A };
      \node[draw] (nb) at (4,0) { Node B };
      \node[draw] (nc) at (6,0) { Node C };

      \draw[-latex] (stack) -- (na);
      \draw[-latex] (na) -- (nb);
      \draw[-latex] (nb) -- (nc);
    \end{tikzpicture}
  \end{figure}
  \bigskip

  Droping the Stack will lead its box to drop the Node A, then other nodes
  recursively (remember: when droped, Box free their memory).
\end{frame}

\section{Going further}

\begin{frame}[fragile]{Iterate over the stack}
  We showed you this syntax:
  \begin{rust}
    let values = vec![1,2,3];
    for v in values {
      // do something with v
    }
  \end{rust}
  \bigskip

  This is allowed because \texttt{Vec} is <<Iterable>>. Would be nice to have
  the same feature with our stack.
\end{frame}

\begin{frame}[fragile]{Iterate over the stack}
  To allow users to iterate over the stack, we can implement an Iterator
  type. For now we just want to inspect the stack without moving it.
  \bigskip

  We will then iterate over references.
  \bigskip

  \begin{rust}
    pub struct Iter<T> {
      next : Option<&Node<T>>,
    }
  \end{rust}

  \pause%
  Ouch, compiler complains... again...\\
  {\color{red}{\texttt{Expected named lifetime parameter}}}
\end{frame}

\begin{frame}[fragile]{Lifetimes}
  To prevent dangling pointers, rust embeds value lifetime in its type-system.
  No reference to a value can outlive it.
  \bigskip

  \begin{rust}
  let ptr;

  {
    let x : i32 = 0;
    ptr = &x;
  }

  // raises a compilation error
  println!("{}", ptr);
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Explicit lifetimes}
  Almost all the time, lifetimes can be inferred.\\
  But in some cases, the compiler needs some help.
  \bigskip

  Lifetime names begin with a quotation mark (\texttt{'a}, \texttt{'static}...)
  \bigskip

  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{rust}
        // cannot infer lifetime
        struct Iter<T> {
          next : Option<&Node<T>>,
        }
      \end{rust}
    \end{column}
    \begin{column}{0.5\textwidth}
      
      \begin{rust}
        // explicit lifetime
        struct Iter<'a, T> {
          next : Option<& 'a Node<T>>,
        }
      \end{rust}
    \end{column}
  \end{columns}
  \bigskip

  This means: The borrowed (referenced) \texttt{Node} must live as our object
  \texttt{Iter}.
\end{frame}

\begin{frame}[fragile]{Implementing Iterator}
  Here is the \texttt{Iterator} trait:
  \bigskip

  \begin{rust}
    trait Iterator {
      type Item;
      fn next(&mut self) -> Option<Self::Item>;
    }
  \end{rust}
  \bigskip

  \texttt{Item} is the type of items we iterate over: 
  \begin{itemize}
    \item \texttt{Vec<i32>} => \texttt{Item = i32}
    \item \texttt{\&[f64]} => \texttt{Item = \&f64}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Implementing Iterator}
  Here we go, let's implement this trait:
  \bigskip

  \begin{rust}
    impl<'a, T> Iterator for Iter<'a, T> {
      type Item = ???;

      fn next(&mut self) -> Option<Self::Item> {
        /* ??? */
      }
    }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Implementing Iterator: solution}
  \begin{rust}
    impl<'a, T> Iterator for Iter<'a, T> {
      type Item = & 'a T;

      fn next(&mut self) -> Option<& 'a T> {
        match self.next {
          Some(node) => {
            self.next = match &node.next {
              Some(n) => Some(&n),
              None => None,
            };
            Some(&node.value)
          },
          None => None
        }
      }
    }
  \end{rust}
\end{frame}
