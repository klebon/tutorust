%! TEX root = tuto.tex

\section{The structure of a Rust project}

\begin{frame}{The structure of a Rust project}
  A Rust project is a package (a \emph{crate} in Rust parlance).

  A crate may be a library or an executable.
  \bigskip

  A crate is composed of many things:
  \smallskip
  \begin{itemize}
    \item Sources (in the \emph{src/} directory)
    \item Tests (in the \emph{tests/} directory)
    \item Documentation (part of the \emph{target/} directory)
    \item More auxiliary files\dots
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{How to create a project}
  To start a Rust project, one does only need one command: \verb|cargo new|.
  \bigskip

  Usually, this command needs only one option:
  \smallskip
  \begin{itemize}
    \item \verb|--bin {project name}| for an executable
    \item \verb|--lib {project name}| for a library
  \end{itemize}

  \bigskip
  This could lead us to the following command line:

  \begin{minted}{bash}
  $ cargo new --lib rusttutorial
  \end{minted}
\end{frame}

\begin{frame}[fragile]{The layout of the project directory}
  This creates a directory that contains all the necessary to start working.
  \bigskip

  \begin{minipage}{0.3\linewidth}
    \begin{verbatim}
rusttutorial/
  |- .gitignore
  |- .git/
  |- Cargo.toml
  \- src/
      \- lib.rs
    \end{verbatim}
  \end{minipage}
  \begin{minipage}{0.69\linewidth}
    \begin{block}{Cargo.toml}
      \begin{minted}{toml}
      [package]
      name = "rusttutorial"
      version = "0.1.0"
      authors = ["Some One <address@mail.net>"]
      edition = "2018"

      [dependencies]
      bitflags = "1.2"
      \end{minted}
    \end{block}
  \end{minipage}
\end{frame}

\begin{frame}[fragile]{The structure of a Rust crate}
  In terms of code, a crate is a self-contained entity (akin to a library).

  A crate organizes code in modules.

  \centering
  \begin{figure}
    \begin{tikzpicture}
      \node[draw] (crate) {MyCrate};
      \node[draw, below right=of crate] (logmod) {Log module};
      \node[draw, below=of logmod] (mathmod) {Math module};
      \node[draw, right=of mathmod] (matrixmod) {Matrix module};
      \node[draw, below=of mathmod] (iomod) {IO module};

      \draw[->,thick] (crate) |- (logmod);
      \draw[->,thick] (crate.south) |- (mathmod.west);
      \draw[->,thick] (crate.south) |- (iomod.west);
      \draw[->,thick] (mathmod) -- (matrixmod);
    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}[fragile]{About modules and visibility}
  Rust cuts projects into modules. To put it simply:

  \begin{itemize}
    \item A file is a module.
    \item A directory with its \emph{mod.rs} file is a module.
  \end{itemize}

  \bigskip
  Submodules must be declared with the \verb|mod| keyword.
  \bigskip

  \begin{block}{module/mod.rs}
    \begin{rust}
    pub mod submodule; // module/submodule.rs
    \end{rust}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Make things public and import them}
  By default, everything is private to its module.

  If one wants to make something public, they have to use \verb|pub|.
  \bigskip

  \begin{rust}
  pub fn public_function() {
  }
  \end{rust}

  \horizontalrule{}
  \bigskip
  To import something from a module, one uses the keyword \verb|use|.
  \bigskip

  \begin{rust}
  use somecrate::somemodule::public_function;
  \end{rust}
\end{frame}

\begin{frame}[fragile]{A little example}
  This tutorial is composed of a few files.\\
  Each of them are exerciseN.rs
  \bigskip

  \begin{itemize}
    \item \emph{lib.rs} the main file of a library
      \begin{itemize}
        \item \emph{exercise1.rs} the module for exercise 1
        \item \emph{exercise2.rs} the module for exercise 2
        \item \dots
      \end{itemize}
  \end{itemize}
  \medskip

  \begin{overprint}
    \horizontalrule{}
    \medskip

    \onslide<2>
    To declare the submodules, \emph{lib.rs} contains the following lines:
    \medskip

    \begin{rust}
      pub mod exercise1;
      pub mod exercise2;
    \end{rust}

    \onslide<3>
    The test files import these modules using the \verb|use| keyword:
    \medskip

    \begin{rust}
    use rusttuto::exercise1;
    \end{rust}
  \end{overprint}
\end{frame}

\begin{frame}[fragile]{How do I compile that?}
  Usually, people use the \emph{cargo} build manager.
  \bigskip

  Cargo has many appreciable features:
  \smallskip
  \begin{itemize}
    \item create a new project with \verb|cargo new|
    \item compile a project with \verb|cargo build|
    \item run tests with \verb|cargo test|
    \medskip
    \item manage dependencies and configure the building process
      \begin{itemize}
        \item via the Cargo.toml configuration file
      \end{itemize}
  \end{itemize}
\end{frame}

\section{How does it look?}

\begin{frame}[fragile]{But before we dive into Rust code}
  The crate you are in provides you with Rust files.\\
  When exercises will come we will tell you which command to run to test the
  validity of your code.
  \bigskip

  But if you want to fiddle some Rust code and run it yourself, use the file
  \verb|examples/sandbox.rs|. Once you coded a simple program in this file,
  write the command \verb|./run_sandbox|.
\end{frame}

\begin{frame}[fragile]{The hello world}
  Let's begin with a simple code:
  \bigskip

  \begin{rust}
  /// The mighty hello world!
  fn main() {
    println!("Hello World!");
  }
  \end{rust}
  \bigskip

  Go ahead, try it!
\end{frame}

\begin{frame}[fragile]{Functions, parameters and return types}
  Functions can take parameters!
  \bigskip

  \begin{rust}
  fn say_hello(name: &str) {
    println!("Hello {}", name);
  }
  \end{rust}

  \pause{}
  \horizontalrule{}
  \bigskip
  They can return values as well.
  \bigskip

  \begin{rust}
  fn three() -> i64 {
    3 // return keyword can be omitted!
  }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Let's declare variables}
  In Rust, variables are declared with the keyword \verb|let|.\\
  Their type can be omitted most of the time.
  \bigskip

  \begin{rust}
  let x = 'x';
  \end{rust}

  \pause{}
  \horizontalrule{}
  \bigskip
  By default, variables are \emph{immutable}.

  To define a mutable variable, one must use \verb|let mut|.
  \bigskip

  \begin{rust}
  let mut x = 5;
  x = 6;
  \end{rust}
\end{frame}

\begin{frame}[fragile]{First exercise}
  Open the file \emph{src/exercise1.rs} and:

  \begin{itemize}
    \item Create a function named five which returns 5 as a \verb|i64|.
    \item Fix the function twelve to make it return 12.
  \end{itemize}

  \begin{overprint}
    \onslide<1>
    \bigskip
    To run the tests, type the command:
    \smallskip

    \begin{verbatim}
    $ cargo test --features exercise1
    \end{verbatim}

    \onslide<2>
    \begin{block}{Correction}
      \begin{rust}
        pub fn five() -> i64 {
          5
        }

        pub fn twelve() -> i64 {
          let mut result = 5;
          result = 12;

          result
        }
      \end{rust}
    \end{block}
  \end{overprint}
\end{frame}

\begin{frame}[fragile]{Let's go back to variables}
  There are many native types in Rust:
  \bigskip

  \begin{minipage}{0.48\linewidth}
    \begin{itemize}
      \item Integer types
        \begin{itemize}
          \item Signed: \verb|i8, i16, i32, i64|
          \item Unsigned: \verb|u8| \dots \verb|u64|
        \end{itemize}
      \item Floating-point numbers: f32, f64
      \item Arrays \verb|[]| and slices \verb|&[]|
      \item String references \verb|&str|
      \item Tuples \verb|(1, "yeah", true)|
      \item And a lot more\dots
    \end{itemize}
  \end{minipage}\hfill\vline\hfill
  \begin{minipage}{0.48\linewidth}
    Rust is statically and strongly typed

    \begin{itemize}
      \item Almost no implicit conversion
    \end{itemize}
    \bigskip

    Convertion from native to native:

    \begin{rust}
    let x: u32 = 4;
    let y = x as i64;
    \end{rust}
  \end{minipage}
\end{frame}

\begin{frame}[fragile]{Arrays}
  Arrays are C arrays but with compile-time embeded length.
  \bigskip

  \begin{rust}
    // an array of 4 i64 words
    let tab1 : [i64; 4] = [0, 1, 2, 3]; 
    // an mutable array of 16 i64 initialized at 0
    let mut tab2 : [i64; 16] = [0; 16];

    // iterate over arrays
    for v in &tab1 {
      println!("{}", v);
    }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Tuples}
  Tuples are structures with unnamed attributes.
  \bigskip

  \begin{rust}
    // define a simple tuple
    let point = (1, 0);

    // define a tuple and name its content
    let (x, y) = (0, 1);

    println!("{} ({}, {})", point, x, y);
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Custom types}
  It is, of course, possible to create your own types.
  Just like C, there are several kinds of types:
  \medskip

  \begin{itemize}
    \item Structures
    \item Enumerations
    \item Type aliases
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Structs}
  Structures in Rust work the same as C.
  \bigskip

  \begin{minipage}[t]{0.4\linewidth}
    \begin{rust}
      pub struct Product {
        pub quantity: usize,
        pub price: f32,
      }
    \end{rust}
  \end{minipage}\hfill\vline\hfill
  \begin{minipage}[t]{0.55\linewidth}
    \begin{rust}
      let product = Product {
        quantity: 3,
        price: 3.50
      };

      let price = product.price;
    \end{rust}
  \end{minipage}

  \bigskip
  The difference lies in the possibility the give attributes a \verb|pub|
  qualifier.
  \begin{itemize}
    \item Attributes are private by default
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Enums}
  Rust enums are not C enums. They are what we call \textbf{tagged enums}.
  \medskip

  A tagged enum is a datatype that exposes many alternative constructors.
  Each constructor may have attributes.
  \bigskip

  \begin{minipage}[t]{0.4\linewidth}
    \begin{rust}
      pub enum Variable {
        Int(i64),
        Float(f64),
        Bool(bool),
        Nil
      }
    \end{rust}
  \end{minipage}\hfill\vline\hfill
  \begin{minipage}[t]{0.55\linewidth}
    \begin{rust}
      let float = Variable::Float(0.5);
      let nil = Variable::Nil;

      use Variable::*;

      let b = Bool(true);
    \end{rust}
  \end{minipage}
\end{frame}

\begin{frame}[fragile]{Actual C enums}
  Rust allows to create C enums using the pragma \verb|#[repr(C)]|.
  \bigskip

  \begin{minipage}[t]{0.4\linewidth}
  \begin{rust}
  #[repr(C)]
  pub enum SoundLevel {
    Low,
    Medium,
    High
  }
  \end{rust}
  \end{minipage}\hfill\vline\hfill
  \begin{minipage}[t]{0.55\linewidth}
    \begin{rust}
    let level = Soundlevel::Medium;

    use SoundLevel::*;

    let lowlevel = Low;
    \end{rust}
  \end{minipage}
\end{frame}

\begin{frame}[fragile]{And things can be generic}
  Types can have type parameters.
  \medskip

  \begin{itemize}
    \item A list of bools~? \verb|List<bool>|
    \item A list of strings~? \verb|List<&str>|
    \item A list of\dots anything~? \verb|List<T>|
  \end{itemize}

  \bigskip
  It is very useful to create a template of a type (typically for collections).
\end{frame}

\begin{frame}[fragile]{Generics in Rust}
  Syntactically, generic types need to be declared through angle brackets.
  \bigskip

  \begin{rust}
    /// A generic type, an option of T
    enum Option<T> {
      None,
      Some(T)
    }

    /// A generic function that accepts an Option of any type T.
    fn print<T>(something: Option<T>) {
      match something {
        None => print!("None"),
        Some(t) => print!("{}", t)
      }
    }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{What does it mean? How to use it?}
  In the previous slide, we introduced the Option type.
  \begin{itemize}
    \item It can hold \emph{some} value
    \item Or none
  \end{itemize}
  \bigskip

  \begin{block}{Useful to express the lack of a value}
    \begin{rust}
      let last = last_element_of(my_array);
    \end{rust}
    \medskip

    If \verb|my_array| is empty, I have no \emph{last value}, I get a
    \verb|None|.\\
    If \verb|my_array| contains values, I have a result, I get
    \verb|Some(value)|.
  \end{block}

  \bigskip
  This works for any type of elements in the array.
\end{frame}

\begin{frame}[fragile]{About conversions}
  Rust provides many ways to convert between data types.

  However, only native types can use the operator \verb|as|
  \begin{itemize}
    \item There is no obvious way to convert custom type for the compiler
    \item Conversions à la \verb|memcpy| are inherently unsafe
  \end{itemize}

  \pause
  \bigskip
  Conversions are done through functions which usually follow the
  convention\footnote{
    https://doc.rust-lang.org/1.0.0/style/style/naming/conversions.html}:
  \smallskip
  \begin{itemize}
    \item \verb|as_type| for almost costless straightforward conversion
    \item \verb|to_type| for costly conversion
    \item \verb|into_type| for conversions that consume the convertee
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{A few examples}
  \begin{rust}
  let string = String::from("Hello");
  let s = string.as_str();

  let string2 = s.to_string();

  let bytes = string2.into_bytes();
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Operations}
  One can combine values by performing operations on values:
  \medskip

  \begin{rust}
  let x: i64 = 5 + 6 - five();
  let y = some_array[0];
  \end{rust}

  \bigskip
  In this regard, Rust is like every other languages.

  Just to take care of types for things to work!
\end{frame}

\begin{frame}{Traits}
  The possibility for a type to perform an operation is not only builtin.

  Rust type system is quite complex (\emph{looking at you, Haskell\dots}).

  \begin{itemize}
    \item The code can be generic
    \item Types are mostly implicit and inferred
    \item But most importantly: they can implement traits!
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Traits?}
  Traits are interfaces that can specify functions or associated types.
  \bigskip

  \begin{minipage}{0.40\linewidth}
    A type can implement a trait.
    \medskip

    Implementations for the functions must be provided.
  \end{minipage}\hfill\vline\hfill
  \begin{minipage}{0.55\linewidth}
    \begin{rust}
    trait IntoStr {
      fn into_str(self) -> &str;
    }
    \end{rust}

    \horizontalrule{}

    \begin{rust}
    impl IntoStr for str {
      fn into_str(self) -> &str {
        &self
      }
    }
    \end{rust}
  \end{minipage}
\end{frame}

\begin{frame}[fragile]{So what?}
  Operators are in fact syntactic sugar for trait methods.
  \bigskip

  \begin{rust}
  3 + 4 == 3.add(4)
  \end{rust}

  \bigskip
  The reason is that the \verb|+| operator is a method of the \verb|Add| trait.
  \medskip

  \begin{rust}
  pub trait Add<Rhs = Self> {
    type Output;

    fn add(self, rhs: Rhs) -> Self::Output;
  }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Auto-implementation}
  Some traits are long to implement (how to log structures, how to test for
  equality...). In order to help developers, Rust has a nice syntax to implement
  automatically these traits:
  \biskip

  \begin{rust}
    #[derive(Debug)]
    struct Point {
      x : i32,
      y : i32,
    };

    fn main() {

      // prints "Point { x:0, y:1 }"
      println!("{:?}", Point { x:0, y:1 });
    }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Exercise 2}
  In the file \emph{exercise2.rs}, you will find the \verb|struct Point|.

  Try to implement the trait \verb|From<(i64, i64)>| for \verb|Point|.
  \bigskip

  This trait has the following interface:
  \smallskip

  \begin{rust}
  trait From<T> {
    fn from(self: T) -> Self;
  }
  \end{rust}

  Try also to implement the trait \texttt{Add}, seen before.
  \bigskip

  Once it is done, try your code using this command:\\
  \verb|cargo test --features exercise2|
\end{frame}

\begin{frame}[fragile]{We can implement types as well}
  Until now, we implemented traits for types.
  \bigskip

  \begin{rust}
  impl Debug for MySuperDuperType {
    // ...
  }
  \end{rust}
  \bigskip

  But we can implement types as well!

  \begin{rust}
  impl MySuperDuperType {
  }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Type implementation}
  Implementing a type provides it with methods.
  \begin{itemize}
    \item A nice way to organize code
    \item Add fonctionnalities to a type
    \item It is like giving a type its very own trait
  \end{itemize}

  \bigskip
  But most importantly: \textbf{Rust is not object oriented}
\end{frame}

\begin{frame}[fragile]{More about implementations}
  Some methods are very common in Rust, even though they have nothing special:
  \medskip

  \begin{rust}
  pub fn new() -> Self;
  \end{rust}
  \medskip

  \verb|new| is the name typically given to constructor methods.\\
  It prevents the programmer from building complex types from scratch.
\end{frame}

\begin{frame}[fragile]{Some control flow}
  Like most languages, Rust provides us with some constructs:
  \bigskip

  \begin{rust}
  if condition { statement }
  if condition { then_clause } else { else_clause }

  loop { statements }
  while condition { statements }
  for identifier in collection { statements }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{More control flow}
  Rust also has more constructs inherited from functional languages:
  \bigskip

  \begin{minipage}{0.4\linewidth}
    \begin{rust}
    match expression {
      case => statement,
      another_case => {
        many_statements
      }
    }
    \end{rust}
  \end{minipage}\hfill
  \begin{minipage}{0.4\linewidth}
    \begin{rust}
    match number {
      0 => "zero",
      1 => "one",
      2 => "two",
      _ => "way too much"
    }
    \end{rust}
  \end{minipage}
\end{frame}

\begin{frame}[fragile]{Most statements are expressions}
  In Rust, \verb|if|, \verb|match| and \verb|loop| statements are
  expressions.\\
  They can be assigned to variables:
  \bigskip

  \begin{rust}
  let sign = if x < 0 { "Negative" } else { "Positive" };
  \end{rust}

  \pause{}
  \horizontalrule{}
  \bigskip
  This helps a lot with conciseness, especially due to the implicit return:
  \medskip

  \begin{rust}
  fn fac(n: u64) -> u64 {
    match n {
      0 | 1 => 1,
      _ => n * fac(n-1)
    }
  }
  \end{rust}
\end{frame}

\begin{frame}[fragile]{Exercise 3}
  Now let's code some classics:
  \bigskip

  \begin{itemize}
    \item The sum function
      \begin{itemize}
        \item Takes an array of integers and sums them up.
      \end{itemize}
    \item The fibonacci function
      \begin{itemize}
        \item $fib(0) = 0$
        \item $fib(1) = 1$
        \item $fib(n) = fib(n - 2) + fib(n - 1)$
      \end{itemize}
  \end{itemize}
  \bigskip

  Everything is in the \emph{exercise3.rs} file.
\end{frame}

\begin{frame}[fragile]{Exercise 3 -- Correction}
  \begin{overprint}
    \onslide<1>
    \begin{itemize}
      \item The sum function
        \begin{itemize}
          \item Takes an array of integers and sums them up
        \end{itemize}
    \end{itemize}
    \bigskip

    \begin{rust}
    pub fn sum(array: &[u64]) -> u64 {
      let mut accumulator = 0;

      for i in array {
        accumulator += i;
      }

      accumulator
    }
    \end{rust}

    \onslide<2>
    \begin{itemize}
      \item The fibonacci function
        \begin{itemize}
          \item $fib(0) = 0$
          \item $fib(1) = 1$
          \item $fib(n) = fib(n - 2) + fib(n - 1)$
        \end{itemize}
    \end{itemize}
    \bigskip

    \begin{rust}
    pub fn fibo(n: u64) -> u64 {
      match n {
        0 => 0,
        1 => 1,
        _ => fibo(n - 2) + fibo(n - 1)
    }
    \end{rust}
  \end{overprint}
\end{frame}
